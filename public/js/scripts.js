$(function () {
	var socket = io.connect('3.16.24.219:80');
	// var socket = io.connect('http://localhost:3000/');

    window.focus();

    $(window).bind('focus', function() {
        socket.emit('getorderbookStartStop', 'start');
    });

    $(window).bind('blur', function() {
        socket.emit('getorderbookStartStop', 'stop');
    });

	socket.on('news', function (data) {
		// console.log(data);
		socket.emit('start', { my: 'data' });
	});		

	// socket.on('messageToDBredisBuy', function (data) {
	// 	console.log(data)
	// 	$("#mainSheet #messageToDBredisBuy").empty();
	// 	let htmltable = "";
	// 	data[0].forEach(function(data1, index, arr) {
	// 		htmltable = htmltable + "<li>" + data1 + "</li>";	
	// 		$("#mainSheet #messageToDBredisBuy").prepend(htmltable);			
	// 	});
	// });

	// socket.on('messageToDBredisSell', function (data) {
	// 	$("#mainSheet #messageToDBredisSell").empty();
	// 	let htmltable = "";
	// 	data[0].forEach(function(data1, index, arr) {
	// 		htmltable = htmltable + "<li>" + data1 + "</li>";	
	// 		$("#mainSheet #messageToDBredisSell").prepend(htmltable);	
	// 	});
		
	// });

	socket.on('sells', function (data) {
		// console.log(data[0]);
		$("#mainSheet #datasStatsSellTable #datasStatsSellBody").empty();
		let amount = 1;
		data[0].forEach(function(data1, index, arr) {	

	        let a = [];
	        data[1][index].split('T').forEach( function(data, index, arr) {
	            a.push(data);
	        });

	        let b = [];
	        a[0].split('-').forEach( function(data, index, arr) {
	            b.push(data);
	        });  


	        let b1 = [];
	        a[1].split('.').forEach( function(data, index, arr) {
	            b1.push(data);
	        }); 

	        let st = '';
	        // st = b[1] + "-" + b[2] + " " + b1[0];
	        st = b1[0];
			
			let color = 'black';
			let row = '';
			if (data[2][index] < 0) {
				color = 'red';
				row = 'glyphicon glyphicon-arrow-down';
			}
			if (data[2][index] > 0) {
				color = 'green';
				row = 'glyphicon glyphicon-arrow-up';
			}

			let htmltable = "";
			htmltable = htmltable + "<tr>";
			htmltable = htmltable + "<td><a href='https://www.binance.com/ru/trade/" + data1.replace('BTC', '_BTC') + "' target='_blank' id=''>" + data1 + "</a></td>";
			htmltable = htmltable + "<td>" + st + "</td><td>" + data[3][index] + "</td>";
			htmltable = htmltable + "<td>" + data[4][index] + "(<span style='color: " + color + ";'>" + data[2][index] + "%</span>)</td>";
			// htmltable = htmltable + "<td>" + data[5][index] + "%</td>";
			htmltable = htmltable + "</tr>";
			// let htmltable = "<tr><td><a href='https://bittrex.com/Market/Index?MarketName=" + data1 + "' target='_blank' id=''>" + data1 + "</a></td><td>" + st + "</td><td>" + data[3][index] + "</td><td>" + data[4][index] + " (<span style='color: " + color + ";'>" + data[2][index] + "%</span>)</td</tr>";
			$("#mainSheet #datasStatsSellTable #datasStatsSellBody").prepend(htmltable);
		});

		// let summaBuy = 0.0;		
	});	

	socket.on('buys', function (data) {
		$("#mainSheet #datasStatsBuyTable #datasStatsBuyBody").empty();
		let amount = 1;
		data[0].forEach(function(data1, index, arr) {	
			if (index <= 24) {
		        let a = [];
		        // let s = dataset.stimeutc.toString();
		        data[1][index].split('T').forEach( function(data, index, arr) {
		            a.push(data);
		        });

		        let b = [];
		        a[0].split('-').forEach( function(data, index, arr) {
		            b.push(data);
		        });  


		        let b1 = [];
		        a[1].split('.').forEach( function(data, index, arr) {
		            b1.push(data);
		        }); 

		        let st = '';
		        // st = b[1] + "-" + b[2] + " " + b1[0];
		        st = b1[0];

				let color = 'black';
				let row = '';
				if (data[2][index] < 0) {
					color = 'red';
					row = 'glyphicon glyphicon-arrow-down';
				}
				if (data[2][index] > 0) {
					color = 'green';
					row = 'glyphicon glyphicon-arrow-up';
				}

				let shortCoin = [];
		        data1.split('-').forEach( function(data1, index1, arr1) {
		            shortCoin.push(data1);
		        });
		        let htmltable = "";
		        htmltable = htmltable + "<tr>";
		        htmltable = htmltable + "<td><a href='https://www.binance.com/ru/trade/" + data1.replace('BTC', '_BTC') + "' target='_blank' id=''>" + data1 + "</a></td>";
		        htmltable = htmltable + "<td>" + st + "</td>";
		        htmltable = htmltable + "<td>" + data[3][index] + "</td>";
		        htmltable = htmltable + "<td><span id='sellNow" + shortCoin[1] + "'>" + data[4][index] + "</span>(<span id='sellNowPersent" + shortCoin[1] + "' style='color: " + color + ";'>" + data[2][index] + "%</span><span class='" + row + "' style='color: " + color + "; font-size: 14px;'></span>)</td>";
		        // htmltable = htmltable + "<td>" + data[6][index] + "%</td>";
		        htmltable = htmltable + "</tr>";
				// let htmltable = "<tr><td><a href='https://bittrex.com/Market/Index?MarketName=" + data1 + "' target='_blank' id=''>" + data1 + "</a></td><td>" + st + "</td><td>" + data[3][index] + "</td><td><span id='sellNow" + shortCoin[1] + "'>" + data[4][index] + "</span> (<span id='sellNowPersent" + shortCoin[1] + "' style='color: " + color + ";'>" + data[2][index] + "%</span><span class='" + row + "' style='color: " + color + "; font-size: 14px;'></span>)</td></tr>";
				$("#mainSheet #datasStatsBuyTable #datasStatsBuyBody").prepend(htmltable);
			}	
		});       
	});

	socket.on('pers', function (data) {
		let profitSell = parseFloat(data);
		// console.log(profitSell.toFixed(2));
		let color = 'black';
		let sym   = '';
		if (profitSell > 0) {
			color = 'green';
			sym = '+';
		}

		if (profitSell < 0) {
			color = 'red';
		}

		$("#profitsell").text(sym + profitSell + '%');
		$("#profitsell").css('color', color);
	});		

	socket.on('perstotal', function (data) {
		let profitSell = parseFloat(data);
		// console.log(profitSell.toFixed(2));
		let color = 'black';
		let sym   = '';
		if (profitSell > 0) {
			color = 'green';
			sym = '+';
		}

		if (profitSell < 0) {
			color = 'red';
		}

		$("#profitsellTotal").text(sym + profitSell + '%');
		$("#profitsellTotal").css('color', color);
	});		

});	
