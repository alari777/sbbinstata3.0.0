FROM node:carbon

# Install software 
RUN apt-get install -y git
RUN npm install forever -g
VOLUME /sbbistata

# Create app directory

# RUN git clone https://alari777@bitbucket.org/alari777/sbbinstata3.0.0.git /sbbistata/

WORKDIR /sbbistata
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
# COPY package*.json ./

# RUN npm install
# RUN npm install forever -g
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
# COPY . .
# COPY start.sh .

EXPOSE 80
CMD git pull origin master && npm install -y && npm start