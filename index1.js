String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

Array.prototype.unset = function(value) {
    if(this.indexOf(value) != -1) { // Make sure the value exists
        this.splice(this.indexOf(value), 1);
    }   
}

var helmet  = require('helmet');
var path    = require('path');
var express = require('express');
var auth    = require('http-auth');
var fs      = require('fs');
var md5     = require('md5');

const pg = require('pg');

var con = new pg.Client({
    host: 'crtp.cr4dbgd0sggy.us-east-2.rds.amazonaws.com',
    port: 5432,
    user: 'root',
    password: 'var562245',
    database: 'cryptocur'
});
con.connect();
const bittrex   = require('node.bittrex.api');
// Only reading
const APIKEY    = 'c7b83627818d4016ba190029625f9ec1';
const APISECRET = 'f6a6cf0644a343a08cef5209737d619d';

// Boevue
// const APIKEY    = '2e4445437be04ef4b9c3531ae5a2d6bf';
// const APISECRET = '14de4a3172c94d41b6c68ceae92dd9bd';


bittrex.options({ 
    'apikey' : APIKEY, 
    'apisecret' : APISECRET, 
    'verbose' : false, 
    'cleartext' : false
});

// Configure basic auth
var basic = auth.basic({
    realm: 'SUPER SECRET STUFF'
}, function(username, password, callback) {
    callback(username == 'admin' && password == 'test123');
});

var app = express();

// Create middleware that can be used to protect routes with basic auth
var authMiddleware = auth.connect(basic);

var server = require('http').Server(app);
var io     = require('socket.io')(server);
// io.set('origins', '18.220.154.71:32777');
io.set('origins', 'http://localhost:3000/');

server.listen(3000);

app.get('/', authMiddleware, function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.use(helmet());
app.use(express.static(path.join(__dirname, 'public')));

var bt;
var date = new Date();
var year  = date.getFullYear();
var month = date.getMonth() + 1;
if (parseFloat(month) < 10) month = "0" + month;
var day   = date.getDate() - 1;
if (parseFloat(day) < 10) day = "0" + day;

var fullDate = year + '-' + month + '-' + day;
// console.log(fullDate);

var promises = [];
var sells    = [];

var persents    = [];
var namesCoin   = [];
var timesClose  = [];
var firstPrice  = [];
var secondPrice = [];

var namesCoinBuys   = [];
var timesCloseBuys  = [];
var persentsBuy     = [];
var firstPriceBuys  = [];
var secondPriceBuys = [];

var promisesArray = [];

var coins = [];
io.on('connection', function(socket) {
    console.log("Now: getorderbook command.");   
    bittrex.websockets.listen(function(data, client) {
        bt    = client;    

        let sqlS = 'SELECT s.market as smarket, s.timeutc as stimeutc, b.ratebuy as bratebuy, s.ratebuy as sratebuy FROM sells as s LEFT JOIN buys as b ON b.quantity = s.quantity and b.market = s.market WHERE (b.ratebuy IS NOT null) ORDER BY s.timeutc DESC LIMIT 10;';
        con.query(sqlS, function(err, result) {
            // result.rows.forEach(function(dataset, index, arr) {
                // console.log(dataset.body);
                // sells.push(dataset.body);
                // io.emit('ticksDatas', sortHTMLtable); 
            // });
            sells       = [];
            persents    = [];
            namesCoin   = [];
            timesClose  = [];
            firstPrice  = [];
            secondPrice = [];
            result.rows.reverse();
            result.rows.forEach(function(dataset, index, arr) {
                
                // sells.push(dataset);
                let per = ( parseFloat(dataset.bratebuy) - parseFloat(dataset.sratebuy) ) / parseFloat(dataset.bratebuy) * 100 ;
                per = per * (-1);
                if (isFinite(per) == true) {
                    persents.push(per.toFixed(2)); 

                    namesCoin.push(dataset.smarket); 

                    // let a = [];
                    // let s = dataset.stimeutc.toString();
                    // s.split('T').forEach( function(data, index, arr) {
                    //     a.push(data);
                    // });

                    // let b = [];
                    // a[0].split('-').forEach( function(data, index, arr) {
                    //     b.push(data);
                    // });  


                    // let b1 = [];
                    // a[1].split('.').forEach( function(data, index, arr) {
                    //     b1.push(data);
                    // }); 

                    // let st = '';
                    // st = b[1] + "-" + b[2] + " " + b1[1];
                    // console.log(a);
                    timesClose.push(dataset.stimeutc); 
                    firstPrice.push(dataset.bratebuy);             
                    secondPrice.push(dataset.sratebuy); 
                    // console.log(dataset);
                }
            });    
            // sells.push(result.rows[0].body);
            // sells.push(result.rows[1].body);
            // sells.push(result.rows[2].body);
            // sells.push(result.rows[3].body);
            // sells.push(result.rows[4].body);
            // sells.push(result.rows[5].body);
            // sells.push(result.rows[6].body);
            // sells.push(result.rows[7].body);
            // sells.push(result.rows[8].body);
            // sells.push(result.rows[9].body);
            // console.log(result.rows[0].body)
        });

        // console.log(sells);
        io.emit("sells", [namesCoin,    timesClose,   persents, firstPrice, secondPrice]); 
        // console.log(" ============ ");

        let sqlB = 'SELECT market, timeutc FROM public.buys ORDER BY timeutc DESC LIMIT 50;';
        con.query(sqlB, function(err, result) {
            coins = [];
            result.rows.forEach(function(dataset, index, arr) {
                if (coins.includes(dataset.market) == false) {
                    let promise = new Promise(function(resolve, reject) {
                        let sql = "SELECT * FROM public.buyssells WHERE (market = '" + dataset.market + "') ORDER BY timeutc DESC LIMIT 1;";
                        con.query(sql, function(err, result) {               
                            result.rows.forEach(function(dataset1, index1, arr1) {
                                if (dataset1.typeord == 'BUY' && coins.length <= 9) {
                                    coins.push(dataset.market);                                        
                                    let goBuy  = true;
                                    
                                    bittrex.getorderbook( { market : dataset.market, type : 'both' }, function(dataMH) {
                                        try {
                                            if (dataMH.result.sell.length != 0) {
                                                    if (goBuy == true) {
                                                        // console.log(dataMH.result.sell[0])
                                                        // console.log("dataset1.typeord: " + dataset.market + ", Time: " + dataset1.timeutc);                           
                                                        namesCoinBuys.push(dataset.market);
                                                        timesCloseBuys.push(dataset1.timeutc);

                                                        let perBuy = ( parseFloat(dataset1.ratebuy) - parseFloat(dataMH.result.sell[0].Rate) ) / parseFloat(dataset1.ratebuy) * 100 ;
                                                        perBuy = perBuy * (-1);
                                                        persentsBuy.push(perBuy.toFixed(2));
                                                        // console.log("perBuy: " + perBuy);
                                                        firstPriceBuys.push(dataset1.ratebuy);
                                                        secondPriceBuys.push(dataMH.result.sell[0].Rate);

                                                        goBuy = false;
                                                        
                                                    }    
                                                    
                                            }
                                                else {
                                                    
                                                }
                                        }
                                            catch (err) {
                                               
                                            }
                                    }); 
                                    
                                    // namesCoinBuys.push(dataset.market);
                                    resolve("result1");
                                }
                                    else resolve("result1");
                            });                
                        });
                    });

                    promisesArray.push(promise);     
                }
            });

            Promise.all(promisesArray).then(function(values) {
                if (values.length != 0) {
                    promisesArray = [];
                    if (namesCoinBuys.length == 10) {
                        console.log(namesCoinBuys);
                        io.emit("buys", [namesCoinBuys, timesCloseBuys, persentsBuy, firstPriceBuys, secondPriceBuys]); 
                        namesCoinBuys   = [];
                        timesCloseBuys  = [];
                        persentsBuy     = [];
                        firstPriceBuys  = [];
                        secondPriceBuys = [];    
                    }                        
                }    
            }); 

            /*
            if (coins.length == 5) {
                coins.forEach(function(dataset, index, arr) {
                    let sql = "SELECT * FROM public.buyssells WHERE (market = '" + dataset + "') ORDER BY timeutc DESC LIMIT 1;";
                    con.query(sql, function(err, result) {               
                        result.rows.forEach(function(dataset1, index1, arr1) {
                            if (dataset1.typeord == 'BUY') {
                                // console.log("dataset1.typeord: " + dataset);
                                let goBuy  = true;
                                let promise = new Promise(function(resolve, reject) {
                                    bittrex.getmarkethistory( { market : dataset }, function(dataMH) {
                                        try {
                                            if (dataMH.result.length != 0 && goBuy == true) { 
                                                console.log("dataset1.typeord: " + dataset + ", Time: " + dataset1.timeutc);                           
                                                namesCoinBuys.push(dataset);
                                                timesCloseBuys.push(dataset1.timeutc);

                                                let perBuy = ( parseFloat(dataMH.PricePerUnit) - parseFloat(dataMH.result[0].Price) ) / parseFloat(dataMH.PricePerUnit) * 100 ;
                                                perBuy = perBuy * (-1);
                                                persentsBuy.push(per.toFixed(2));

                                                firstPriceBuys.push(dataset1.ratebuy);
                                                secondPriceBuys.push(dataset1.ratebuy);
                                                goBuy = false;
                                                resolve("result1");
                                            }
                                        }
                                            catch (err) {
                                                resolve("result1");
                                            }
                                    }); 
                                });
                                promisesArray.push(promise);    
                            }
                        });

                        Promise.all(promisesArray).then(function(values) {
                            if (values.length != 0) {
                                promiseDateArray = [];
                                if (namesCoinBuys.length == 5) {
                                    console.log("YES");
                                    io.emit("buys", [namesCoinBuys,    timesCloseBuys,   persentsBuy, firstPriceBuys, secondPriceBuys]); 
                                    namesCoinBuys   = [];
                                    timesCloseBuys  = [];
                                    persentsBuy     = [];
                                    firstPriceBuys  = [];
                                    secondPriceBuys = [];                             
                                }                        
                            }    
                        });                 
                    });    
                });             
            }    
            */
        }); 

        client.serviceHandlers.disconnected = (websocket) => {
            console.log("bittrex disconnected");
            process.exit(0);
        };

        client.serviceHandlers.onerror = (websocket) => {
            console.log("bittrex onerror");
            process.exit(0);
        };

    }); 


    setInterval ( function() { 
    if (typeof bt === "undefined" || typeof con === "undefined" || con === null) {
        process.exit(0);
    }
    }, 20000);
});