String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

Array.prototype.unset = function(value) {
    if(this.indexOf(value) != -1) { // Make sure the value exists
        this.splice(this.indexOf(value), 1);
    }   
}

var helmet  = require('helmet');
var path    = require('path');
var express = require('express');
var auth    = require('http-auth');
var fs      = require('fs');
var md5     = require('md5');

const pg = require('pg');

var con = new pg.Client({
    host: 'crtp.cr4dbgd0sggy.us-east-2.rds.amazonaws.com',
    port: 5432,
    user: 'root',
    password: 'var562245',
    database: 'cryptocur'
});
con.connect();

// db 0 -- bittrex sim
// db 1 -- binance sim
// db 2 -- bittrex alex prod
const Redis = require('ioredis');
const redis = new Redis({
    port:     6379,
    host:     '35.229.54.166',
    password: 'var777',
    db:       1
});

// const redis1 = new Redis({
//     port:     6379,
//     host:     '18.216.72.28',
//     password: 'var777',
//     db:       1
// });

const redis7 = new Redis({
    port:     6379,
    host:     '35.227.49.228',
    password: 'var777',
    db:       1
});

const redis8 = new Redis({
    port:     6379,
    host:     '35.243.139.227',
    password: 'var777',
    db:       1
});

const redis9 = new Redis({
    port:     6379,
    host:     '35.196.56.57',
    password: 'var777',
    db:       1
});

const redis10 = new Redis({
    port:     6379,
    host:     '35.231.26.232',
    password: 'var777',
    db:       1
});

const bittrex   = require('node.bittrex.api');
// Only reading
// const APIKEY    = 'c7b83627818d4016ba190029625f9ec1';
// const APISECRET = 'f6a6cf0644a343a08cef5209737d619d';

// Boevue
// const APIKEY    = '2e4445437be04ef4b9c3531ae5a2d6bf';
// const APISECRET = '14de4a3172c94d41b6c68ceae92dd9bd';
const APIKEY    = '9f78113c3f194bd4943181565703b751';
const APISECRET = 'b39d383f946048e1a34b2b8d60773457';

bittrex.options({ 
    'apikey' : APIKEY, 
    'apisecret' : APISECRET, 
    'verbose' : false, 
    'cleartext' : false
});

// Configure basic auth
var basic = auth.basic({
    realm: 'SUPER SECRET STUFF'
}, function(username, password, callback) {
    callback(username == 'admin' && password == 'test123');
});

var app = express();

// Create middleware that can be used to protect routes with basic auth
var authMiddleware = auth.connect(basic);

var server = require('http').Server(app);
var io     = require('socket.io')(server);
// io.set('origins', '18.220.154.71:32772');
// io.set('origins', 'localhost:3000');
// io.set('origins', '3.16.24.219:32773');
server.listen(80);

app.get('/', authMiddleware, function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.use(helmet());
app.use(express.static(path.join(__dirname, 'public')));

var bt;
var date = new Date();
var year  = date.getFullYear();
var month = date.getMonth() + 1;
if (parseFloat(month) < 10) month = "0" + month;
var day   = date.getDate() - 1;
if (parseFloat(day) < 10) day = "0" + day;

var fullDate = year + '-' + month + '-' + day;
// console.log(fullDate);

var promises = [];
var sells    = [];

var persents    = [];
var namesCoin   = [];
var timesClose  = [];
var firstPrice  = [];
var secondPrice = [];
var notes       = [];

var persentsk    = [];
var namesCoink  = [];
var timesClosek  = [];
var firstPricek  = [];
var secondPricek = [];
var notesk       = [];

var namesCoinBuys   = [];
var timesCloseBuys  = [];
var persentsBuy     = [];
var firstPriceBuys  = [];
var secondPriceBuys = [];
var namesCoinBuysk   = [];
var timesCloseBuysk  = [];
var persentsBuyk     = [];
var firstPriceBuysk  = [];
var secondPriceBuysk = [];

var perBuySumArr1 = [];
var perBuySumArr2 = [];
var perBuySumArr1k = [];
var perBuySumArr2k = [];


var promisesArray = [];
var promisesArrayk = [];

var coins        = [];
var coinsk       = [];
var persents  = [];
var persentsk = [];

var messageToDBredis     = [];
var messageToDBredisBuy  = [];
var messageToDBredisSell = [];
var messageToDBredisk     = [];
var messageToDBredisBuyk  = [];
var messageToDBredisSellk = [];

var perBuySum  = 0.0;
var perBuySumk = 0.0;

var signals = [];

var resultPers12 = 0;
var resultPers22 = 0;

var resultPers12k = 0;
var resultPers22k = 0;

io.on('connection', function(socket) {
    console.log("Now: getorderbook command.");   
    bittrex.websockets.listen(function(data, client) {

        function buySellNowPrice(host, symbol) {
            return new Promise( (resolve, reject) => {
                // let red = new Redis({
                //     port:     6379,
                //     host:     host,
                //     password: 'var777',
                //     db:       1
                // });

                host.pipeline([
                    ['get', symbol + ':buysellnow']
                ]).exec(function (err, results) { 
                    if (err) reject(err);

                    if (results[0][1] !== null) 
                        resolve(results[0][1]);
                    else
                        resolve(null);
                })               
            });
        }

        async function getNowPrices(symbol) {
            try {
                let res1 = await buySellNowPrice(redis7, symbol);
                if (res1 !== null) {
                    return res1;
                }

                let res2 = await buySellNowPrice(redis8, symbol);
                if (res2 !== null) {
                    return res2;
                }

                let res3 = await buySellNowPrice(redis9, symbol);
                if (res3 !== null) {
                    return res3;
                }

                let res4 = await buySellNowPrice(redis10, symbol);
                if (res4 !== null) {
                    return res4;
                }

                return null;
            }
                catch (error) {
                    return error;
                }
        }

        bt    = client; 
        // kadet home
        let sqlPK = "SELECT ( (ratebuy - ratesell) / ratebuy * (-100) ) as per FROM public.bsnew_sim_binance_mar_total_sim WHERE (typeordbuy = 'BUY' AND typeordsell = 'SELL' AND (timeutcsell >= now() - interval '26 hours') ) ORDER BY timeutcsell DESC";
        con.query(sqlPK, function(err, result) {
            let persentsD = [];
            if (result.rowCount >= 1) { 
                result.rows.forEach(function(dataset, index, arr) {  
                    let perWithComision = parseFloat(dataset.per) - ( Math.abs(parseFloat(dataset.per) * 0.15) );
                    persentsD.push(perWithComision); 
                });  

                // let resultPers1 = 0;
                resultPers12k = 0;
                if (persentsD.length != 0) {
                    resultPers12k = persentsD.reduce(function(sum, current) {
                      return sum + current;
                    }, 0);
                }
                    else resultPers12k = 0;  

                // io.emit("persk", resultPers1.toFixed(2));
            }    
        });       
        

        let sqlPTotalK = "SELECT ( (ratebuy - ratesell) / ratebuy * (-100) ) as per FROM public.bsnew_sim_binance_mar_total_sim WHERE (typeordbuy = 'BUY' AND typeordsell = 'SELL' AND (timeutcsell >= now() - interval '43802 hours') ) ORDER BY timeutcsell DESC";
        con.query(sqlPTotalK, function(err, result) {
            let persentsD = [];
            if (result.rowCount >= 1) { 
                result.rows.forEach(function(dataset, index, arr) {  
                    let perWithComisionTotal = parseFloat(dataset.per) - ( Math.abs(parseFloat(dataset.per) * 0.15) );
                    persentsD.push(perWithComisionTotal); 
                });  

                // let resultPers2 = 0;
                resultPers22k = 0;
                if (persentsD.length != 0) {
                    resultPers22k = persentsD.reduce(function(sum, current) {
                      return sum + current;
                    }, 0);
                }
                    else resultPers22k = 0;  

                // io.emit("perstotalk", resultPers2.toFixed(2));
            }    
        }); 
       
        // kadet end

        let sqlP = "SELECT ( (ratebuy - ratesell) / ratebuy * (-100) ) as per FROM public.bsnew_sim_binance_total WHERE (typeordbuy = $1 AND typeordsell = $2 AND (timeutcsell >= now() - interval '26 hours') ) ORDER BY timeutcsell DESC";
        con.query(sqlP, ['BUY', 'SELL'], function(err, result) {
            let persentsD = [];
            if (result.rowCount >= 1) { 
                result.rows.forEach(function(dataset, index, arr) {  
                    let perWithComision = parseFloat(dataset.per) - ( Math.abs(parseFloat(dataset.per) * 0.15) );
                    persentsD.push(perWithComision); 
                });  

                // let resultPers1 = 0;
                resultPers12 = 0;
                if (persentsD.length != 0) {
                    resultPers12 = persentsD.reduce(function(sum, current) {
                      return sum + current;
                    }, 0);
                }
                    else resultPers12 = 0;  

                // io.emit("pers", resultPers1.toFixed(2));
            }    
        });       
        

        let sqlPTotal = "SELECT ( (ratebuy - ratesell) / ratebuy * (-100) ) as per FROM public.bsnew_sim_binance_total WHERE (typeordbuy = 'BUY' AND typeordsell = 'SELL' AND (timeutcsell >= now() - interval '43802 hours') ) ORDER BY timeutcsell DESC";
        // let sqlPTotal = "SELECT ( (ratebuy - ratesell) / ratebuy * (-100) ) as per FROM public.bsnew_sim_binance_total WHERE (typeordbuy = 'BUY' AND typeordsell = 'SELL' AND (timeutcsell >= now() - interval '30 days') ) ORDER BY timeutcsell DESC"; 
        con.query(sqlPTotal, function(err, result) {
            let persentsD = [];
            if (result.rowCount >= 1) { 
                result.rows.forEach(function(dataset, index, arr) {  
                    let perWithComisionTotal = parseFloat(dataset.per) - ( Math.abs(parseFloat(dataset.per) * 0.15) );
                    persentsD.push(perWithComisionTotal); 
                });  

                // let resultPers22 = 0;
                resultPers22 = 0;
                if (persentsD.length != 0) {
                    resultPers22 = persentsD.reduce(function(sum, current) {
                      return sum + current;
                    }, 0);
                }
                    else resultPers22 = 0;  

                // io.emit("perstotal", resultPers2.toFixed(2));
            }    
        }); 

        // kadet home
        let sqlLLk = "SELECT * FROM public.bsnew_sim_binance_mar_total_sim WHERE (typeordbuy = $1 AND typeordsell = $2) ORDER BY timeutcsell DESC LIMIT 25";
        con.query(sqlLLk, ['BUY', 'SELL'], function(err, result) {
            sellsk       = [];
            persentsk    = [];
            namesCoink   = [];
            timesClosek  = [];
            firstPricek  = [];
            secondPricek = [];
            notesk      = [];
            let counter = 0;
            result.rows.reverse();
            result.rows.forEach(function(dataset, index, arr) {
                let per = ( parseFloat(dataset.ratebuy) - parseFloat(dataset.ratesell) ) / parseFloat(dataset.ratebuy) * 100 ;
                per = per * (-1);
                per = parseFloat(per) - ( Math.abs(per * 0.15) );
                if (isFinite(per) == true) {
                    persentsk.push(per.toFixed(2));
                    namesCoink.push(dataset.market);
                    timesClosek.push(dataset.timeutcsell);
                    firstPricek.push(dataset.ratebuy);
                    secondPricek.push(dataset.ratesell);
                    // notes.push('-');
                }
                counter++;
            }); 
        });        

        io.emit("sellsk", [namesCoink, timesClosek, persentsk, firstPricek, secondPricek]);         
        // kadet end
        
        let sqlLL = "SELECT * FROM public.bsnew_sim_binance_total WHERE (typeordbuy = $1 AND typeordsell = $2) ORDER BY timeutcsell DESC LIMIT 25";
        con.query(sqlLL, ['BUY', 'SELL'], function(err, result) {
            sells       = [];
            persents    = [];
            namesCoin   = [];
            timesClose  = [];
            firstPrice  = [];
            secondPrice = [];
            notes       = [];
            let counter = 0;
            result.rows.reverse();
            result.rows.forEach(function(dataset, index, arr) {
                let per = ( parseFloat(dataset.ratebuy) - parseFloat(dataset.ratesell) ) / parseFloat(dataset.ratebuy) * 100 ;
                per = per * (-1);
                per = parseFloat(per) - ( Math.abs(per * 0.15) );
                if (isFinite(per) == true) {
                    persents.push(per.toFixed(2));
                    namesCoin.push(dataset.market);
                    timesClose.push(dataset.timeutcsell);
                    firstPrice.push(dataset.ratebuy);
                    secondPrice.push(dataset.ratesell);
                    // notes.push('-');
                }
                counter++;
            }); 
        });        

        // io.emit("sells", [namesCoin, timesClose, persents, firstPrice, secondPrice, notes]); 
        io.emit("sells", [namesCoin, timesClose, persents, firstPrice, secondPrice]); 

        messageToDBredisBuy = [];
        io.emit("messageToDBredisBuy", [messageToDBredisBuy]);

        messageToDBredisSell = [];
        io.emit("messageToDBredisSell", [messageToDBredisSell]);

        let sqlB23 = 'SELECT * FROM public.signals_sim_binance_mar_total_sim ORDER BY timeutccreated DESC LIMIT 5;';
        con.query(sqlB23, function(err, result) {
            messageToDBredisSell = [];
            let counter = 0;
            signals = [];
            if (result.rowCount >= 1) {
                result.rows.forEach(function(dataset, index, arr) {
                    signals.push(dataset.text1);
                    if ( signals.length == result.rowCount ) {
                        signals.reverse();
                        io.emit("signals", [signals]);
                    }
                });
            }
                else io.emit("signals", []);
        });


        let sqlB = 'SELECT * FROM public.bsnew_sim_binance_total WHERE (typeordbuy = $1 AND typeordsell = $2) ORDER BY timeutcbuy DESC LIMIT 15;';
        con.query(sqlB, ['BUY', ''], function(err, result) {
            coins = [];
            perBuySum = 0.0;
            result.rows.forEach(function(dataset, index, arr) {
                let promise = new Promise( async (resolve, reject) => {
                    let resPriceNow = null;
                    try {
                        resPriceNow = await getNowPrices(dataset.market);
                    }
                        catch (error) {

                        }
                    if (coins.includes(dataset.market) == false) {
                        coins.push(dataset.market);  
                        let goBuy  = true;
                        redis.pipeline([
                            ['get', dataset.market + ':buysellnow']
                        ]).exec( (err, results) => {

                            if (results[0][1] !== null) {
                                try {
                                    let buysellnow = parseFloat(results[0][1]);                                    
                                    if (buysellnow != 0.0) { 
                                        if (goBuy == true) {                         
                                            namesCoinBuys.push(dataset.market);
                                            timesCloseBuys.push(dataset.timeutcbuy);

                                            let perBuy = ( parseFloat(dataset.ratebuy) - parseFloat(buysellnow) ) / parseFloat(dataset.ratebuy) * 100 ;
                                            perBuy = perBuy * (-1);
                                            perBuy = parseFloat(perBuy) - ( Math.abs(perBuy * 0.075) );
                                            perBuySum = perBuySum + perBuy;
                                            perBuySumArr1.push(perBuySum);
                                            // messageToDBredis.push(results[1][1]);
                                            messageToDBredis.push("a");

                                            persentsBuy.push(perBuy.toFixed(2));
                                            // console.log("perBuy: " + perBuy);
                                            firstPriceBuys.push(dataset.ratebuy);
                                            secondPriceBuys.push(buysellnow);

                                            goBuy = false;
                                            resolve([dataset.market, dataset.timeutcbuy, perBuy.toFixed(2), dataset.ratebuy, buysellnow, perBuySum, dataset.note, messageToDBredis]);
                                        }    
                                            // else resolve("result1");
                                    }
                                        else {
                                            resolve("error1");
                                        }
                                }
                                    catch (err) {
                                        resolve("result2");
                                    } 
                            }
                                else {
                                    // redis1.pipeline([
                                    //     ['get', dataset.market + ':buysellnow']
                                    // ]).exec(function (err, results) { 
                                        // if (results[0][1] !== null) {
                                        if (resPriceNow !== null) {
                                            try {
                                                let buysellnow = parseFloat(resPriceNow); //parseFloat(results[0][1]);                                                
                                                if (buysellnow != 0.0) { 
                                                    if (goBuy == true) {                        
                                                        namesCoinBuys.push(dataset.market);
                                                        timesCloseBuys.push(dataset.timeutcbuy);

                                                        let perBuy = ( parseFloat(dataset.ratebuy) - parseFloat(buysellnow) ) / parseFloat(dataset.ratebuy) * 100 ;
                                                        perBuy = perBuy * (-1);
                                                        perBuy = parseFloat(perBuy) - ( Math.abs(perBuy * 0.075) );
                                                        perBuySum = perBuySum + perBuy;
                                                        perBuySumArr1.push(perBuySum);
                                                        // messageToDBredis.push(results[1][1]);
                                                        messageToDBredis.push("a");

                                                        persentsBuy.push(perBuy.toFixed(2));
                                                        // console.log("perBuy: " + perBuy);
                                                        firstPriceBuys.push(dataset.ratebuy);
                                                        secondPriceBuys.push(buysellnow);

                                                        goBuy = false;
                                                        resolve([dataset.market, dataset.timeutcbuy, perBuy.toFixed(2), dataset.ratebuy, buysellnow, perBuySum, dataset.note, messageToDBredis]);
                                                    }    
                                                        else resolve("result3");
                                                }
                                                    else {
                                                        resolve("error1");
                                                    }
                                            }
                                                catch (err) {
                                                    resolve("result4");
                                                } 
                                        }  
                                            else resolve("result5");                                                  
                                    // });                                               
                                }                                 
                        });
                    }
                        else resolve("result6");
                });

                promisesArray.push(promise);
            });

            Promise.all(promisesArray).then(function(values) {
                let arr0 = [];
                let arr1 = [];
                let arr2 = [];
                let arr3 = [];
                let arr4 = [];
                let arr5 = [];
                let arr6 = [];
                let arr7 = [];
                values.forEach(function(dataset, index, arr) {
                    if (Array.isArray(dataset)) {
                        if (arr0.includes(dataset[0]) == false && arr0.length <= 24) {
                            arr0.push(dataset[0]);
                            arr1.push(dataset[1]);
                            arr2.push(dataset[2]);
                            arr3.push(dataset[3]);
                            arr4.push(dataset[4]);                        
                            arr5.push(dataset[5]);
                            arr6.push(dataset[6]);
                            arr7.push(dataset[7]);
                        }                         
                    }                         
                });
                promisesArray = [];
                namesCoinBuys   = [];
                timesCloseBuys  = [];
                persentsBuy     = [];
                firstPriceBuys  = [];
                secondPriceBuys = []; 
                coins           = [];
                perBuySumArr1   = [];
                messageToDBredis = [];

                arr0.reverse();
                arr1.reverse();
                arr2.reverse();
                arr3.reverse();
                arr4.reverse();
                arr5.reverse();
                arr6.reverse();
                arr7.reverse();
                // console.log(arr0.length);              
                io.emit("buys", [arr0, arr1, arr2, arr3, arr4, arr5, arr6, arr7]); 
                io.emit("pers", (resultPers12 + perBuySum).toFixed(2));
                io.emit("perstotal", (resultPers22 + perBuySum).toFixed(2));
            }); 
        }); 

        // kadet home
        let sqlBk = 'SELECT * FROM public.bsnew_sim_binance_mar_total_sim WHERE (typeordbuy = $1 AND typeordsell = $2) ORDER BY timeutcbuy DESC LIMIT 20;';
        con.query(sqlBk, ['BUY', ''], function(err, result) {
            coinsk = [];
            perBuySumk = 0.0;
            
            // let resPriceNow = null;
            // result.rows.forEach(async function(dataset, index, arr) {
            //     resPriceNow = await getNowPrices(dataset);
            // });
            
            result.rows.forEach(function(dataset, index, arr) {                
                // let res1 = await buySellNowPrice('34.73.231.128', symbol);
                let promisek = new Promise( async (resolve, reject) => {
                    let resPriceNow = null;
                    try {
                        resPriceNow = await getNowPrices(dataset.market);
                        // console.log(dataset.market, resPriceNow);
                    }
                        catch (error) {

                        }
                    if (coinsk.includes(dataset.market) == false) {
                        coinsk.push(dataset.market);  
                        let goBuy  = true;
                        
                        redis.pipeline([
                            ['get', dataset.market + ':buysellnow']
                        ]).exec( (err, results) => {

                            if (results[0][1] !== null) {
                                try {
                                    let buysellnow = parseFloat(results[0][1]);  
                                    // console.log(dataset.market, buysellnow);                                  
                                    if (buysellnow != 0.0) { 
                                        if (goBuy == true) {                         
                                            namesCoinBuysk.push(dataset.market);
                                            timesCloseBuysk.push(dataset.timeutcbuy);

                                            let perBuy = ( parseFloat(dataset.ratebuy) - parseFloat(buysellnow) ) / parseFloat(dataset.ratebuy) * 100 ;
                                            perBuy = perBuy * (-1);
                                            perBuy = parseFloat(perBuy) - ( Math.abs(perBuy * 0.075) );
                                            perBuySumk = perBuySumk + perBuy;
                                            perBuySumArr1k.push(perBuySumk);
                                            // messageToDBredisk.push(results[1][1]);
                                            messageToDBredisk.push("a");

                                            persentsBuyk.push(perBuy.toFixed(2));
                                            // console.log("perBuy: " + perBuy);
                                            firstPriceBuysk.push(dataset.ratebuy);
                                            secondPriceBuysk.push(buysellnow);

                                            goBuy = false;
                                            resolve([dataset.market, dataset.timeutcbuy, perBuy.toFixed(2), dataset.ratebuy, buysellnow, perBuySumk, dataset.note, messageToDBredisk]);
                                        }    
                                            else resolve("r");
                                    }
                                        else {
                                            resolve("e");
                                        }
                                }
                                    catch (err) {
                                        resolve("r");
                                    } 
                            }
                                else {
                                    // redis1.pipeline([
                                    //     ['get', dataset.market + ':buysellnow']
                                    // ]).exec(function (err, results) {
                                        // if (results[0][1] !== null) {
                                        
                                        if (resPriceNow !== null) {
                                            try {
                                                let buysellnow = parseFloat(resPriceNow); // parseFloat(results[0][1]);                                                
                                                if (buysellnow != 0.0) { 
                                                    if (goBuy == true) {                        
                                                        namesCoinBuysk.push(dataset.market);
                                                        timesCloseBuysk.push(dataset.timeutcbuy);

                                                        let perBuy = ( parseFloat(dataset.ratebuy) - parseFloat(buysellnow) ) / parseFloat(dataset.ratebuy) * 100 ;
                                                        perBuy = perBuy * (-1);
                                                        perBuy = parseFloat(perBuy) - ( Math.abs(perBuy * 0.075) );
                                                        perBuySumk = perBuySumk + perBuy;
                                                        perBuySumArr1k.push(perBuySumk);
                                                        // messageToDBredisk.push(results[1][1]);
                                                        messageToDBredisk.push("a");

                                                        persentsBuyk.push(perBuy.toFixed(2));
                                                        // console.log("perBuy: " + perBuy);
                                                        firstPriceBuysk.push(dataset.ratebuy);
                                                        secondPriceBuysk.push(buysellnow);

                                                        goBuy = false;
                                                        resolve([dataset.market, dataset.timeutcbuy, perBuy.toFixed(2), dataset.ratebuy, buysellnow, perBuySumk, dataset.note, messageToDBredisk]);
                                                    }    
                                                        else resolve("r");
                                                }
                                                    else {
                                                        resolve("e");
                                                    }
                                            }
                                                catch (err) {
                                                    resolve("r");
                                                } 
                                        }  
                                            else resolve("r");                                                  
                                    // });                                               
                                }                                 
                        });
                    }
                        else resolve("result6");
                });

                promisesArrayk.push(promisek);
            });

            Promise.all(promisesArrayk).then(function(values) {                
                let arr0 = [];
                let arr1 = [];
                let arr2 = [];
                let arr3 = [];
                let arr4 = [];
                let arr5 = [];
                let arr6 = [];
                let arr7 = [];
                values.forEach(function(dataset, index, arr) {                    
                    if (Array.isArray(dataset)) {
                        if (arr0.includes(dataset[0]) == false && dataset[0] !== 'e' && dataset[0] !== 'r' && arr0.length <= 24) {
                            arr0.push(dataset[0]);
                            arr1.push(dataset[1]);
                            arr2.push(dataset[2]);
                            arr3.push(dataset[3]);
                            arr4.push(dataset[4]);                        
                            arr5.push(dataset[5]);
                            arr6.push(dataset[6]);
                            arr7.push(dataset[7]);
                        }                         
                    }                         
                });

                promisesArrayk = [];
                namesCoinBuysk   = [];
                timesCloseBuysk  = [];
                persentsBuyk     = [];
                firstPriceBuysk  = [];
                secondPriceBuysk = []; 
                coinsk           = [];
                perBuySumArr1k   = [];
                messageToDBredisk = [];

                arr0.reverse();
                arr1.reverse();
                arr2.reverse();
                arr3.reverse();
                arr4.reverse();
                arr5.reverse();
                arr6.reverse();
                arr7.reverse();
                // console.log(arr0.length);              
                io.emit("buysk", [arr0, arr1, arr2, arr3, arr4, arr5, arr6, arr7]); 
                io.emit("persk", (resultPers12k + perBuySumk).toFixed(2));                
                io.emit("perstotalk", (resultPers22k + perBuySumk).toFixed(2));
            }); 
        }); 
        // kadet end

        client.serviceHandlers.disconnected = (websocket) => {
            console.log("bittrex disconnected");
            process.exit(0);
        };

        client.serviceHandlers.onerror = (websocket) => {
            console.log("bittrex onerror");
            process.exit(0);
        };

    }); 


    setInterval ( function() { 
    if (typeof bt === "undefined" || typeof con === "undefined" || con === null) {
        process.exit(0);
    }
    }, 20000);
});